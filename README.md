<!--
# Universidad Nacional Autónoma de México
# Dirección General de Cómputo y Tecnologías de Información y Comunicación
# CSI/UNAM-CERT
# Plan de Becarios de Seguridad en Cómputo
-->
# Administración y Seguridad en Linux

- https://PBSC-AdminUNIX.gitlab.io/
- https://gitlab.com/PBSC-AdminUNIX/pbsc-adminunix.gitlab.io
- https://aula.cert.unam.mx/course/view.php?id=7

- [Lista de reproducción de YouTube 📼][youtube-playlist] con material complementario

[youtube-playlist]: https://www.youtube.com/playlist?list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry
