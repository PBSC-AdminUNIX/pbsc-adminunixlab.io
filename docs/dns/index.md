# Registro de dominio

| ![](img/FreeNom-001.png) |
|:------------------------:|
| Página de inicio de FreeNom |

| ![](img/FreeNom-002.png) |
|:------------------------:|
| Inicio de sesión en FreeNom |

| ![](img/FreeNom-003.png) |
|:------------------------:|
| Menú de registro de dominio |

| ![](img/FreeNom-004.png) |
|:------------------------:|
| Búsqueda del nombre de dominio |

| ![](img/FreeNom-005.png) |
|:------------------------:|
| Disponibilidad del nombre de dominio |

| ![](img/FreeNom-006.png) |
|:------------------------:|
| Ajustes de duración y servidores NS |

| ![](img/FreeNom-007.png) |
|:------------------------:|
| Completar pedido |

| ![](img/FreeNom-008.png) |
|:------------------------:|
| Confirmación de la orden |

| ![](img/FreeNom-009.png) |
|:------------------------:|
| Menú de dominios registrados |

| ![](img/FreeNom-010.png) |
|:------------------------:|
| Administrar dominio |

| ![](img/FreeNom-011.png) |
|:------------------------:|
| Editar servidores NS (_glue records_) |

| ![](img/FreeNom-012.png) |
|:------------------------:|
| Utilizar servidores NS personalizados |

| ![](img/FreeNom-013.png) |
|:------------------------:|
| Menú `WHOIS` |

| ![](img/FreeNom-014.png) |
|:------------------------:|
| Búsqueda WHOIS |

| ![](img/FreeNom-015.png) |
|:------------------------:|
| Resultados WHOIS |

--------------------------------------------------------------------------------

Búsqueda DNS

- Registros `NS`

```
$ dig NS tonejito.cf.

; <<>> DiG 9.10.6 <<>> NS tonejito.cf.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 54078
;; flags: qr rd ra; QUERY: 1, ANSWER: 4, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;tonejito.cf.			IN	NS

;; ANSWER SECTION:
tonejito.cf.		172800	IN	NS	ns-942.awsdns-53.net.
tonejito.cf.		172800	IN	NS	ns-36.awsdns-04.com.
tonejito.cf.		172800	IN	NS	ns-1775.awsdns-29.co.uk.
tonejito.cf.		172800	IN	NS	ns-1352.awsdns-41.org.

;; Query time: 190 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Oct 04 19:20:21 CDT 2021
;; MSG SIZE  rcvd: 179
```

- Registro `SOA`

```
$ dig SOA tonejito.cf.

; <<>> DiG 9.10.6 <<>> SOA tonejito.cf.
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 28499
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;tonejito.cf.			IN	SOA

;; ANSWER SECTION:
tonejito.cf.		900	IN	SOA	ns-942.awsdns-53.net. awsdns-hostmaster.amazon.com. 1 7200 900 1209600 86400

;; Query time: 495 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Mon Oct 04 20:21:22 CDT 2021
;; MSG SIZE  rcvd: 124
```
