---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Administración y Seguridad en Linux
summary: Plan de Becarios en Seguridad Informática - CSI/UNAM-CERT
authors:
  - Andrés Leonardo Hernández Bermúdez
  - Dante Erik Santiago Rodríguez Pérez
  - Enrique Tezozomoc Perez Campos
date: 2021-09-29
---
<!--
# Universidad Nacional Autónoma de México
# Dirección General de Cómputo y Tecnologías de Información y Comunicación
# Coordinación de Seguridad de la Información //UNAM-CERT
# Plan de Becarios en Seguridad Informática
-->
# Administración y Seguridad en Linux

- 29 de septiembre al 18 de octubre de 2021
- 14:00 a 18:00 hrs
- 40 horas

## Instructores

- <span title="tonejito">Andrés Leonardo Hernández Bermúdez</span>
- <span title="santiagodantes">Dante Erik Santiago Rodríguez Pérez</span>
- <span title="merrovingo">Enrique Tezozomoc Perez Campos</span>

## Evaluación

Elemento   | Valor
:---------:|:----:
Asistencia | 10%
Proyecto   | 90%

>>>
Se considera la participación como extra para la calificación de este curso
>>>

## Material

- [Página del curso][pagina-curso]

- [Grupo de GitLab][grupo-gitlab]

- Se compartirán algunos materiales en la carpeta de [elementos adjuntos][moodle-attachments] dentro del [curso de moodle][moodle]

- Hay una [lista de reproducción de YouTube 📼][youtube-playlist] con material complementario

--------------------------------------------------------------------------------

## Temas

#### Día 1

Miércoles, 29 de septiembre de 2021

- [Presentación 📼][video-presentacion]

- Historia de Linux y UNIX
    - [Diapositivas 📝][diapositivas-historia-unix]
    - [Video 📼][video-historia-unix]


- Proceso de inicio de Linux y SystemD
    - [Diapositivas 📝][diapositivas-inicio-linux]
    - [Diapositivas CSC 📄][diapositivas-init]
    - [Video 📼][video-inicio-linux]

- Estructura de directorios LSB-FHS
    - [Diapositivas 📝][diapositivas-directorios]
    - [Diapositivas CSC 📄][diapositivas-fhs-fstab]
    - [Video 📼][video-directorios]

[video-presentacion]: https://www.youtube.com/watch?v=CD0ka3Xg-30&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=1
[diapositivas-historia-unix]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Diapositivas%20AdminLinux/PBSC_Admin_Linux_01-02-Historia-UNIX.pdf?forcedownload=1
[video-historia-unix]: https://www.youtube.com/watch?v=t6faJyGB2aY&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=5
[diapositivas-inicio-linux]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Diapositivas%20AdminLinux/PBSC_Admin_Linux_01-03-Proceso_de_inicio_de_Linux.pdf?forcedownload=1
[diapositivas-init]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-02-init.pdf?forcedownload=1
[video-inicio-linux]: https://www.youtube.com/watch?v=-P8iKoForGU&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=6
[diapositivas-directorios]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Diapositivas%20AdminLinux/PBSC_Admin_Linux_01-04-Estructura_de_directorios.pdf?forcedownload=1
[diapositivas-fhs-fstab]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-01-fhs-fstab.pdf?forcedownload=1
[video-directorios]: https://www.youtube.com/watch?v=dfAw_uINQ2E&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=7

##### Tarea día 1

- Crear cuenta de GitLab asociada a un correo electónico (no usar Google, FB, Twitter, etc.)

- Máquinas virtuales
    - Debian `10` _buster_ u `11` _bullseye_
    - CentOS `8` u `8` _Stream_
- Ver los siguientes videos:

    - Configuración de red

        - Manual: [ifconfig / ip 📼][video-red-manual]
        - Persistente: [/etc/network/interfaces y /etc/sysconfig/network-scripts 📼][video-red-persistente]
        - [Tabla de rutas 📼][video-rutas]

[video-red-manual]: https://www.youtube.com/watch?v=H74s4_oJNYY&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=8
[video-red-persistente]: https://www.youtube.com/watch?v=UErZ4i9XmLM&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=98
[video-rutas]: https://www.youtube.com/watch?v=ekI0vQx8E2g&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=10

--------------------------------------------------------------------------------

#### Día 2

Jueves, 30 de septiembre de 2021

- Usuarios
    - [Diapositivas CSC 📄][diapositivas-usuarios]

- Permisos del sistema de archivos
    - [Diapositivas CSC 📄][diapositivas-permisos]
    - [Video 📼][video-permisos]

- `sudo`
    - [Diapositivas CSC 📄][diapositivas-sudo]

- CRON
    - [Diapositivas CSC 📄][diapositivas-cron]

- Cuotas
    - [Diapositivas CSC 📄][diapositivas-cuotas]

[diapositivas-usuarios]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-03-users.pdf?forcedownload=1
[diapositivas-permisos]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-05-perms.pdf?forcedownload=1
[video-permisos]: https://www.youtube.com/watch?v=1pHS0gUjH7g&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=11
[diapositivas-sudo]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-04-admin.pdf?forcedownload=1
[diapositivas-cron]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-06-cron.pdf?forcedownload=1
[diapositivas-cuotas]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-07-quota.pdf?forcedownload=1

##### Tarea día 2

- Agregar una segunda interfaz _host-only_ a las máquinas virtuales **Debian** y **CentOS** de tal manera que puedan alcanzarse entre sí

    - En VMware parece que esto se hace de manera automática con la interfaz de red que se asigna a las máquinas virtuales

    - En [este video 📼][redes-arp] viene explicado como funciona en VirtualBox (ver hasta `4:15`)

    - En [este sitio][oracle-blog-virtualbox] viene como agregar la interfaz _host-only_ en VirtualBox

- Revisar el [video de Permisos de sistemas de archivos][video-permisos] y hacer el ejercicio con los [scripts][scripts-permisos]

- Revisar las diapositivas de `sudo` e intentar hacer una política

- Hacer un _script_ de `bash` que tenga una funcionalidad similar al comando [`newusers`][man-newusers]

    - Entrega: **Domingo 3 de octubre 23:59 hrs**

    - Cada equipo hará un _script_, mismo que puede usar para crear los usuarios locales que se piden en su parte del [proyecto]

    - El _script_ acepta un argumento que es el archivo de entrada

    - El archivo de entrada contiene una o mas lineas con el siguiente formato

        - El separador de los campos es `:`

        - `usuario`:`contraseña`:`uid`:`gid`:`gecos`:`home`:`shell`

    - El _script_ lee el archivo de entrada

        - Verificar si existe el grupo asignado en el campo `gid`

            - Imprime un mensaje de error en `STDERR` y continua con el siguiente usuario en caso de que el grupo no exista

        - Crea el usuario si no existe e imprimie un mensaje a `STDOUT`

            - En caso de que el usuario exista, el _script_ imprime una línea de advertencia a `STDERR` y continúa con el siguiente usuario

            - Después de agregar el usuario crear el directorio home y asignar el _shell_ especificado

        - Considerar el uso de herramientas como [`adduser`][man-adduser], [`usermod`][man-usermod], [`passwd`][man-passwd], [`mkpasswd`][man-mkpasswd], etc. para no implementar la funcionalidad desde cero

[man-adduser]: https://linux.die.net/man/8/adduser
[man-usermod]: https://linux.die.net/man/8/usermod
[man-passwd]: https://linux.die.net/man/1/passwd
[man-mkpasswd]: https://linux.die.net/man/1/mkpasswd

[redes-arp]: https://youtu.be/bqNLVQDqmLk
[oracle-blog-virtualbox]: https://blogs.oracle.com/scoter/post/oracle-vm-virtualbox-networking-options-and-how-to-manage-them#Host-only
[scripts-permisos]: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/codigo-ejemplo/-/tree/master/filesystems/files
[man-newusers]: https://linux.die.net/man/8/newusers

--------------------------------------------------------------------------------

#### Día 3

Viernes, 1 de octubre de 2021

- SSH
    - [Diapositivas CSC 📄][diapositivas-ssh]
    - [Video 📼][video-ssh]

- SFTP con `ChrootDirectory`
    - [Diapositivas CSC 📄][diapositivas-sftp]

- Firewall IPtables
    - [Diapositivas CSC 📄][diapositivas-iptables]
    - [Video 📼][video-iptables]

- NFS
    - [Video 📼][video-nfs]

[diapositivas-ssh]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-08-sshd.pdf?forcedownload=1
[video-ssh]: https://www.youtube.com/watch?v=Hnu7BHBDcoM&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=12
[diapositivas-sftp]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T07%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20I/2015-hardening-10-sftp.pdf?forcedownload=1
[diapositivas-iptables]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/CSC2015/T08%20-%20Hardening%20en%20sistemas%20operativos%20Linux%20II/2015-hardening-11-fw-iptables.pdf?forcedownload=1
[video-iptables]: https://www.youtube.com/watch?v=6lYnadL60Cs&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=13
[video-nfs]: https://www.youtube.com/watch?v=AcKG6UsAO-Y&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=14

##### Tarea día 3

- Replicar la [práctica de SFTP][diapositivas-sftp] y en la máquina Debian o CentOS y conectarse con FileZilla desde la computadora física

- Revisar el video de la [configuración de NFS][video-nfs] y hacer la implementación donde una de las máquinas virtuales sea el servidor y otra el cliente

- Revisar el video de [control de versiones con Git y GitLab 📼][video-git]

    - Crear el archivo `README.md` en el repositorio de su [proyecto] donde se listen los nombres de los integrantes

    - Agregar alguna imagen al directorio `img` y referenciarla en el archivo `README.md`

        - Revisar esta [sección de documentación](git) para más información

[video-git]: https://www.youtube.com/watch?v=sLSqGjY84Sg&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=2

--------------------------------------------------------------------------------

#### Día 4

Lunes, 4 de octubre de 2021

- MySQL / MariaDB

    - [Diapositivas 📄][diapositivas-mysql]

- PostgreSQL

    - [Diapositivas 📄][diapositivas-postgresql]

[diapositivas-mysql]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Diapositivas%20AdminLinux/PBSI_Admin_Linux-MySQL.pdf?forcedownload=1
[diapositivas-postgresql]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Diapositivas%20AdminLinux/PBSI_Admin_Linux-PostgreSQL.pdf?forcedownload=1

##### Tarea día 4

- Crear una cuenta en [FreeNom][sitio-freenom] **asociada a una cuenta de correo**

    - Puede ser el correo `@bec.cert.unam.mx` o cualquier otro correo de una institución educativa o dominio _personalizado_
        - El sitio de FreeNom tiene problemas con los correos de dominios _genéricos_ como `gmail.com`, `yahoo.com`, `hotmail.com`, `outlook.com`, etc.
    - No utilizar el botón de _inicio de sesión con Google_ ni FB

- Crear una cuenta en [HE.net][sitio-he_net] asociada a <u>la misma cuenta de correo del punto anterior</u>

[sitio-freenom]: https://my.freenom.com/clientarea.php
[sitio-he_net]: https://dns.he.net/

--------------------------------------------------------------------------------

#### Día 5

Martes, 5 de octubre de 2021

- Registro de dominios
    - [Video 📼][video-whois]
    - [Diapositivas 📄][diapositivas-whois]
    - [Material escrito 📝][material-whois]
    - <https://howdns.works/>

- Protocolo DNS
    - [Video 📼][video-dns]
    - [Material escrito 📝][material-dns]

[video-whois]: https://www.youtube.com/watch?v=WC2YzuCkBgQ&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=15
[video-dns]: https://www.youtube.com/watch?v=r4PntflJs9E&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=16
[diapositivas-whois]: dns/
[material-whois]: http://redes-ciencias-unam.gitlab.io/temas/whois/
[material-dns]: http://redes-ciencias-unam.gitlab.io/temas/dns/

--------------------------------------------------------------------------------

#### Día 6

Miércoles, 6 de octubre de 2021

- HTTP

    - Configuración Apache HTTPD en Debian
        - [Video 📼][video-apache-debian]

    - Directivas de configuración de Apache HTTPD
        - [Video 📼][video-apache-config]

    - VirtualHost local con /etc/hosts
        - [Video 📼][video-apache-vhost-local]

    - VirtualHost con registros DNS
        - [Video 📼][video-apache-vhost-dns]

#### Día 7

Jueves, 7 de octubre de 2021

- Certificados SSL

    - Teoría X509
        - [Video 📼][video-ssl-x509]

    - Certificado autofirmado
        - [Video  📼][video-ssl-selfsign]

    - Certificado con Let's Encrypt
        - [Video  📼][video-ssl-certbot]

- HTTPS

    - <https://howhttps.works/>

[video-apache-debian]: https://www.youtube.com/watch?v=XbQ_dBuERdM&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=17
[video-apache-config]: https://www.youtube.com/watch?v=3JkQs3KcjxQ&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=18
[video-apache-vhost-local]: https://www.youtube.com/watch?v=ZnqSNXIr-h4&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=19
[video-apache-vhost-dns]: https://www.youtube.com/watch?v=JYo5rc4mhf0&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=20
[video-ssl-x509]: https://www.youtube.com/watch?v=rXqkJi_FTuQ&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=21
[video-ssl-selfsign]: https://www.youtube.com/watch?v=66dOHHD6L5I&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=22
[video-ssl-certbot]: https://www.youtube.com/watch?v=kpiChLT5JPs&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=23

--------------------------------------------------------------------------------

#### Día 8

Viernes, 8 de octubre de 2021

- SMTP
    - [Video 📼][video-smtp]
    - [Material escrito SMTP 📝][material-smtp]
    - [Material escrito SMTP - DNS 📝][material-smtp-dns]

- Postfix
    - [Video 📼][video-postfix]

[material-smtp]: https://redes-ciencias-unam.gitlab.io/temas/smtp/
[material-smtp-dns]: https://redes-ciencias-unam.gitlab.io/temas/smtp-dns/
[video-smtp]: https://www.youtube.com/watch?v=xm_wYT4GOnU&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=24
[video-postfix]: https://www.youtube.com/watch?v=ox11vzqNP4k&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=26

#### Día 9

Lunes, 11 de octubre de 2021

- LDAP
    - [Video 📼][video-ldap]

- OpenLDAP
    - Práctica: [Autenticación centralizada con OpenLDAP 📄][practica-ldap]
    - [Video 📼][video-openldap]

[practica-ldap]: https://aula.cert.unam.mx/pluginfile.php/967/mod_folder/content/0/Congreso%20de%20Seguridad%20en%20C%C3%B3mputo/AdminUNAM%202011/Autenticacion-centralizada-con-OpenLDAP.pdf?forcedownload=1
[video-ldap]: https://www.youtube.com/watch?v=oTizxuVNMWI&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=27
[video-openldap]: https://www.youtube.com/watch?v=EKI7Y7KklKM&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=28

--------------------------------------------------------------------------------

#### Día 10

Martes, 12 de octubre de 2021

- VPN
    - [Video 📼][video-vpn]

- OpenVPN
    - [Video 📼][video-openvpn]

[video-vpn]: https://www.youtube.com/watch?v=kEmDhrkRIOw&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=29
[video-openvpn]: https://www.youtube.com/watch?v=ucSqZG-voxE&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=30

#### Día 11

Miércoles, 13 de octubre de 2021

- IMAP

- Dovecot

- Configuración de envío de correo con Postfix y autenticación SASL
    - [Documentación de Debian 📚][debian-postfix-sasl]
    - [Solución al problema en StackOverflow 🤔][stackoverflow-postfix-sasl]

[debian-postfix-sasl]: https://wiki.debian.org/PostfixAndSASL
[stackoverflow-postfix-sasl]: https://stackoverflow.com/questions/43931621/not-able-to-authenticate-smtp-clients-on-debianpostfixsasl-with-rimap

--------------------------------------------------------------------------------

#### Día 12

Jueves, 14 de octubre de 2021

- Revisión intermedia del [proyecto]

#### Día 13

Viernes, 15 de octubre de 2021

- Revisión intermedia del [proyecto]

--------------------------------------------------------------------------------

#### Día 14

Lunes, 18 de octubre de 2021

- Revisión final del [proyecto]

--------------------------------------------------------------------------------

[pagina-curso]: https://PBSC-AdminUNIX.gitlab.io/
[grupo-gitlab]: https://gitlab.com/PBSC-AdminUNIX/
[proyecto]: ./proyecto
[moodle]: https://aula.cert.unam.mx/course/view.php?id=7
[moodle-attachments]: https://aula.cert.unam.mx/mod/folder/view.php?id=296
[youtube-playlist]: https://tinyurl.com/PBSC-AdminUNIX-YT
