---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
title: Proyecto de curso
summary: Plan de Becarios en Seguridad Informática - CSI/UNAM-CERT
authors:
- Andrés Leonardo Hernández Bermúdez
- Dante Erik Santiago Rodríguez Pérez
- Enrique Tezozomoc Perez Campos
date: 2021-09-29
---
<!--
# Universidad Nacional Autónoma de México
# Dirección General de Cómputo y Tecnologías de Información y Comunicación
# CSI/UNAM-CERT
# Plan de Becarios de Seguridad en Cómputo
-->
# Administración y Seguridad en Linux

## Proyecto

Instalar una infraestructura distribuida en una nube pública que contemple distintos servicios de red implementados en servidores GNU/Linux conectados a través de una VPN y que tengan un dominio DNS y certificados SSL válidos.

## Diagrama

| ![](img/diagrama.png)                 |
|:-------------------------------------:|
| Diagrama de dependencias de servicios |

## Condiciones

- No cambiar la contraseña de `root`
- No quitar las llaves existentes en `~/.ssh/authorized_keys`
    - Cambiar propietario y permisos para que funcione la autenticación SSH con llaves
        - `chown -R usuario:grupo ~/.ssh`
        - `chmod -c 0700 ~/.ssh`
        - `chmod -c 0600 ~/.ssh/authorized_keys*`
- Usuarios y grupos
    - No quitar ni modificar usuarios existentes
    - Crear un usuario normal `becarios` y habilitar `sudo` sin contraseña con `NOPASSWD`
    - Crear un usuario normal para **cada persona del equipo** (ej. `andres-hernandez`)
        - Agregar su llave pública para autenticación SSH
        - Agregar a los usuarios a un _grupo privilegiado_ (`sudo` o `wheel`) para dar privilegios administrativos
- Tener cuidado con las reglas de `iptables`
    - Utilizar `iptables-apply` para evitar _cerrar la puerta_
    - Considerar el uso de `iptables-persistent`, `ufw` o `firewalld`
- Tener cuidado en la configuración del servicio `ssh`
- Permitir **siempre** las conexiones de los siguientes equipos
    - `tonejito.cf`
    - `becarios.tonejito.cf`
    - `priv.becarios.tonejito.cf`
    - Rango de VPN `10.0.8.0/24` (revisar esto con el equipo responsable del servicio)
    - Direcciones IP de las máquinas de los otros equipos
    - Rangos IP de RedUNAM `132.247.0.0/16` y `132.248.0.0/16`

## Conexión a los equipos

- Los equipos tienen configurado un usuario llamado `becarios` el cual tiene permisos de `sudo`
- El usuario tiene configurada la llave SSH que cada integrante del equipo generó
- La conexión a cada equipo es mediante SSH en el puerto `22` y se puede utilizar el nombre provisional de host o la dirección IP
    - Los nombres de los equipos son: `dns`, `web`, `mail`, `vpn`, `directory` y `storage`
    - Cada equipo tiene un nombre DNS provisional bajo el dominio `admin-linux.tonejito.cf` y el equipo de NS deberá crear el nombre de host bajo el dominio `planbecarios15.cf`

```
$ ssh -i ruta/hacia/llave_rsa becarios@1.2.3.4
$ ssh -i ruta/hacia/llave_rsa becarios@HOST.planbecarios15.cf
```

- Se recomienda generar un archivo de configuración para el cliente de SSH en [~/.ssh/config][ssh-config] con el siguiente contenido:

```
#	~/.ssh/config

Host *
  TCPKeepAlive yes
  ServerAliveInterval 10

Host admin-linux.tonejito.cf *.admin-linux.tonejito.cf  3.21.51.197 dns.planbecarios15.cf   18.117.38.39 web.planbecarios15.cf   3.20.143.7 mail.planbecarios15.cf   18.189.117.97 vpn.planbecarios15.cf   3.14.238.180 directory.planbecarios15.cf   13.58.149.147 storage.planbecarios15.cf
  Port 22
  User becarios
  IdentityFile ~/.ssh/keys/becarios_rsa
  StrictHostKeyChecking accept-new
  HashKnownHosts no
  UserKnownHostsFile ~/.ssh/known_hosts2
```

[ssh-config]: files/ssh_config

## Monitoreo

Los equipos serán monitoreados a través de **Nagios**

- <https://nagios.becarios.tonejito.info/nagios3/map.php>
- <https://nagios.becarios.tonejito.info/nagios3/?corewindow=/cgi-bin/nagios3/status.cgi?host=all>

>>>
Preguntar por las credenciales de acceso en el <span title="PBSC-AdminUNIX">_chat de Telegram_</span>
>>>

--------------------------------------------------------------------------------

## Equipos

### Servicios públicos

#### DNS

| Documentación	| <https://gitlab.com/PBSC-AdminUNIX/2021/dns#readme> |
|:-------------:|:----------------:|
| OS		| Amazon Linux 2 |
| Arquitectura  | ARM64 |
| Host		| `ns.planbecarios15.cf` |
| Hostname	| `dns.admin-linux.tonejito.cf` |
| IP		| `3.21.51.197` |
| Usuario	| `ec2-user` |

- Dominio DNS
    - Registrar el dominio DNS `planbecarios15.cf` y apuntar los _glue records_ a los servidores DNS primario y secundario
    - Verificar que los cambios se vean reflejados en la base de datos de WHOIS para que se liste el servidor DNS primario y secundario
- Instalar **BIND** desde paquetes con `yum`
    - Este equipo será el servidor DNS primario y autoritativo (master) de la zona `planbecarios15.cf`
    - Abrir el puerto `53` (domain) por `TCP` y `UDP`
    - Versionar los cambios de la zona DNS en su repositorio de GitLab
    - Permitir las búsquedas recursivas **únicamente** desde las direcciones IP _públicas_ de los demás servidores y el segmento de VPN.
    - Permitir la replicación **únicamente** desde el servidor DNS secundario
    - Ayudar a los demás equipos a configurar los DNS primario y secundario en [`/etc/resolv.conf`](https://linux.die.net/man/5/resolv.conf) e incluir el dominio de búsqueda
- Registros DNS
    - Verificar todos los registros creados con `dig` al servidor DNS primario y secundario
    - El TTL de **todos** los registros excepto `SOA` DNS será de 5 minutos
    - Crear registro `SOA` apuntando a `hostmaster@planbecarios15.cf`
    - Crear registros `NS` para los servidores DNS primario y secundario
    - Crear registros `SRV` para los servicios de todos los equipos
    - Crear registros `MX` y `SPF` para el servidor de correo
    - Crear registros `CAA` y `CNAME` para validar el certificado _wildcard_ emitido por Let's Encrypt
    - Crear registros `A`, `PTR` y `CNAME` para todos los servidores
        - Un registro `A` por servidor apuntando a la IP pública (ej. web.`planbecarios15.cf`)
            - El registro `A` principal del dominio debe apuntar al servidor `web`
            - `planbecarios15.cf` => web.`planbecarios15.cf`
        - Registros `CNAME` apuntando a cada nombre común de los servidores
            - Los otros equipos podrán solicitar más registros DNS de acuerdo a sus necesidades

| Elemento                      | Valor                       |
|------------------------------:|:---------------------------:|
|         **dns**.`planbecarios15.cf` |        **ns**.`planbecarios15.cf` |
|        **smtp**.`planbecarios15.cf` |      **mail**.`planbecarios15.cf` |
|        **imap**.`planbecarios15.cf` |      **mail**.`planbecarios15.cf` |
|         **www**.`planbecarios15.cf` |       **web**.`planbecarios15.cf` |
|       **mysql**.`planbecarios15.cf` |       **web**.`planbecarios15.cf` |
|     **mariadb**.`planbecarios15.cf` |       **web**.`planbecarios15.cf` |
|  **postgresql**.`planbecarios15.cf` |       **web**.`planbecarios15.cf` |
|        **ldap**.`planbecarios15.cf` | **directory**.`planbecarios15.cf` |
|         **nfs**.`planbecarios15.cf` |   **storage**.`planbecarios15.cf` |


--------------------------------------------------------------------------------

#### Mail

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2021/mail#readme> |
|:-------------:|:-----:|
| OS		| CentOS 7 |
| Arquitectura  | ARM64 |
| Host		| `mail.planbecarios15.cf` |
| Hostname	| `mail.admin-linux.tonejito.cf` |
| IP		| `3.20.143.7` |
| Usuario	| `centos` |

- Instalar **Postfix** y **Dovecot** desde paquetes con `yum`
    - Deben autenticar a los usuarios desde **LDAP**
    - Los buzones de correo se deben guardar en el servidor **NFS**
        - El equipo **storage** deberá exportar el directorio `/srv/home`
        - El equipo **directory** deberá configurar el directorio `/srv/home/%u` como el *home* de cada usuario de **LDAP**
    - Pedir al equipo de DNS que haga los registros `MX` y `SPF`
    - Instalar el certificado _wildcard_ SSL emitido por Let's Encrypt en Postfix y Dovecot
    - Crear las siguientes listas de correo en `/etc/aliases`
        - El alias de correo debe enviar los mensajes a los correos `@bec.cert.unam.mx` de los integrantes de cada equipo

| Dirección                     | Destinatarios      |
|------------------------------:|:-------------------|
|      `hostmaster@planbecarios15.cf` | Equipo `dns`       |
|       `webmaster@planbecarios15.cf` | Equipo `web`       |
|      `postmaster@planbecarios15.cf` | Equipo `mail`      |
| `directorymaster@planbecarios15.cf` | Equipo `directory` |
|   `storagemaster@planbecarios15.cf` | Equipo `storage`   |
|       `vpnmaster@planbecarios15.cf` | Equipo `vpn`       |

- Abrir los puertos `25/tcp` (smtp), `465/tcp` (smtps) y `587/tcp` (submission)
- Abrir el puerto `993/tcp` (imaps)
- Permitir el relay desde las direcciones IP públicas y privadas de todos los demás equipos
- Instalar **Roundcube** y configurar para enviar correo
    - No importa si es desde `.tar.gz`, con `git` o desde paquetes utilizando `yum`
    - No importa si PHP se configura como módulo de Apache o como FPM
- Les ayudará bastante configurar el repositorio **EPEL**
- Debe configurarse la conexión a **LDAP** para que se reconozcan los usuarios

--------------------------------------------------------------------------------

#### Web

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2021/web#readme> |
|:-------------:|:-----:|
| OS		| Debian 10 |
| Arquitectura  | ARM64 |
| Host		| `web.planbecarios15.cf` |
| Hostname	| `web.admin-linux.tonejito.cf` |
| IP		| `18.117.38.39` |
| Usuario	| `admin` |

- Instalar **Apache 2.4** desde paquetes con `apt`
    - Todos los sitios web deben tener su propio subdominio
    - Forzar el tráfico por HTTPS, ya sea con redireción 301 o con HSTS
- Instalar el servidor de **MariaDB** y **PostgreSQL** locales
    - Configurar el certificado SSL emitido por Let's Encrypt en MariaDB y PostgreSQL
- Certificado SSL
    - Todos los VirtualHosts deberán tener un certificado SSL emitido por Let's Encrypt
        - Se recomienda hacer uso de un certificado SSL _wildcard_ y compartirlo con los demás equipos
        - Pedir al equipo DNS que haga los registros `CNAME` necesarios para validar el dominio
        - Se recomienda instalar y configurar `certbot` para la renovación automática del certificado
    - Crear un script para respaldar el certificado y la llave privada en `/srv/ssl`
- Instalar **PHP 7.4 FPM** desde el repositorio **Sury**
    - <https://deb.sury.org/>
    - Configurar el _handler_ **FastCGI** de Apache
- Instalar **WordPress** 5.x
    - No importa si es desde `.tar.gz`, con `git` o paquetes utilizando `apt`
    - WordPress debe conectarse a la base de datos **MariaDB** _local_
- Instalar **Redmine** 4.x
    - No importa si se instala `ruby`, `rails` y `passenger` con paquetes con `apt` o utilizando `rvm` y `gem install`
    - Redmine debe conectarse a la base de datos **PostgreSQL** _local_
- Instalar **SquirrelMail**
    - No importa si es desde `.tar.gz`, paquetes con `apt` o con `git`
    - Configurarlo para que se conecte a Postfix y Dovecot del equipo **mail**

--------------------------------------------------------------------------------

### Servicios privados

#### Storage

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2021/storage#readme> |
|:-------------:|:-----:|
| OS		| Ubuntu 20.04 LTS |
| Arquitectura  | ARM64 |
| Host		| `storage.planbecarios15.cf` |
| Hostname	| `storage.admin-linux.tonejito.cf` |
| IP		| `13.58.149.147` |
| Usuario	| `ubuntu` |

- Instalar y configurar el servidor de **NFS** v4
    - Exportar el directorio `/srv/ssl` para guardar el certificado SSL que tramitó el equipo **web**
    - Exportar el directorio `/srv/home` para guardar los buzones de correo del equipo **mail**
    - Exportar un directorio bajo `/srv/pki` por cada equipo para guardar las llaves privadas y los certificados de VPN de cada servidor e integrantes de ese equipo
    - Exportar el directorio `/srv/www`
        - El equipo **web** lo montará en `/var/www` y guardará los _htdocs_ de `redmine` y `wordpress` ahí
- Usuarios de NFS
    - Debe configurarse la conexión a **LDAP** para que se guarden adecuadamente los buzones de correo
    - Configurar el propietario del directorio `/srv/www` como el usuario `www-data`
        - Verificar el usuario `www-data` tenga los mismos *uid* y *gid* en el equipo **web** y **storage**
        - Considerar el uso de las directivas `all_squash`, `anonuid` y `anongid`, ver [`man 5 exports`](https://linux.die.net/man/5/exports)
- Instalar y configurar **BackupPC** desde paquetes con `apt`
    - Configurar el VirtualHost con un certificado SSL _wildcard_ emitido por Let's Encrypt
    - Utilizar `backuppc` para **copiar** los respaldos de otros equipos
        - Directorio LDAP (archivo ldif) generado por el equipo **directory**
        - Respaldos en formato SQL de MariaDB y PostgreSQL generado por el equipo **web**
        - Clonar y hacer pull del repositorio de la zona DNS
    - Guardar los respaldos en `/srv/backup` y crear un directorio por cada servicio que se respalda
        - `ssl`
        - `ldap`
        - `mariadb`
        - `postgresql`
        - `dns`

--------------------------------------------------------------------------------

#### Directory

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2021/directory#readme> |
|:-------------:|:-----:|
| OS		| Debian 11 |
| Arquitectura  | ARM64 |
| Host		| `directory.planbecarios15.cf` |
| Hostname	| `directory.admin-linux.tonejito.cf` |
| IP		| `3.14.238.180` |
| Usuario	| `admin` |

- Instalar **OpenLDAP** desde paquetes utilizando `apt`
- Instalar **LDAP Account Manager** y **LDAP Toolbox Self Service**
    - Configurar el VirtualHost con un certificado SSL emitido por Let's Encrypt
    - No importa si es desde `.tar.gz`, paquetes con `apt` o con `git`
    - No importa si PHP se configura como módulo de Apache o como FPM
- Apoyar a los equipos que requieran configurar el cliente LDAP en el sistema operativo o en algúna aplicación

--------------------------------------------------------------------------------

#### VPN

| Documentación | <https://gitlab.com/PBSC-AdminUNIX/2021/vpn#readme> |
|:-------------:|:-----:|
| OS		| CentOS 8 Stream |
| Arquitectura  | ARM64 |
| Host		| `vpn.planbecarios15.cf` |
| Hostname	| `vpn.admin-linux.tonejito.cf` |
| IP		| `18.189.117.97` |
| Usuario	| `centos` |

<!--
- Instalar el servidor de **MariaDB** v10.6 y **PostgreSQL** v14
    - Configurar el certificado SSL emitido por Let's Encrypt en MariaDB y PostgreSQL
    - Desde el repositorio oficial utilizando paquetes RPM para `yum`
        - <https://dev.mysql.com/downloads/repo/yum/>
        - <https://dev.mysql.com/doc/mysql-yum-repo-quick-guide/en/>
        - <https://mariadb.com/kb/en/yum/>
        - <https://downloads.mariadb.org/mariadb/repositories/>
        - <https://mariadb.com/kb/en/mariadb-package-repository-setup-and-usage/>
        - <https://mariadb.com/resources/blog/how-to-install-mariadb-on-rhel8-centos8/>
        - <https://mariadb.com/docs/deploy/deployment-methods/repo/>
        - <https://yum.postgresql.org/>
        - <https://yum.postgresql.org/repopackages.php>
        - <https://wiki.postgresql.org/wiki/YUM_Installation>
    - Este equipo tendrá el servidor `master` de MariaDB y tendrá que coordinarse con el equipo **storage** para que configuren la réplica
- Instalar `phpMyAdmin` y `phpPgAdmin`
    - Configurar el VirtualHost con un certificado SSL emitido por Let's Encrypt
    - No importa si es desde `.tar.gz`, paquetes con `yum` o con `git`
    - No importa si PHP se configura como módulo de Apache o como FPM
-->
- Instalar **OpenVPN** desde paquetes con `yum`
    - Este equipo será el servidor de VPN
    - Este equipo tendrá la CA raíz que firmará los certificados de cliente para la VPN
    - Montar el directorio `/srv/pki` del equipo **storage** y guardar:
        - La configuración de OpenVPN para los clientes
        - Certificados y llaves privadas para cada cliente
- Les ayudará bastante configurar el repositorio **EPEL**
    - <https://fedoraproject.org/wiki/EPEL>

--------------------------------------------------------------------------------

<style type="text/css">
table {
  width: 100%;
  margin: 2em auto 2em auto;
}
th {
  padding: 1em;
}
</style>
