# Ejemplo README.md

Integrantes

- NOMBRE
- NOMBRE
	...

## Encabezado nivel 2

Texto de muestra

### Encabezado nivel 3

Imagen de muestra

![Imagen de ejemplo](img/muestra.png)

#### Encabezado nivel 4

Liga hacia script

- [`script.sh`](files/script.sh)

##### Encabezado nivel 5

Tabla

| Encabezado | Otro encabezado | Uno mas    |
|:----------:|:---------------:|:----------:|
| Contenido  | Más contenido   | Otra celda |
| Contenido  | Más contenido   | Otra celda |
| Contenido  | Más contenido   | Otra celda |
