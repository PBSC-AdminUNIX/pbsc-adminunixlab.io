---
# https://www.mkdocs.org/user-guide/writing-your-docs/#meta-data
date: 2021-10-01
title: Control de versiones con git
summary: Plan de Becarios en Seguridad Informática - CSI/UNAM-CERT
authors:
  - Andrés Leonardo Hernández Bermúdez
---
# Control de versiones con `git`

Este contenido también está disponible en otros formatos

- [Video 📼][video-git]
- [Diapositivas 📝][diapositivas-git]

[video-git]: https://www.youtube.com/watch?v=sLSqGjY84Sg&list=PLbiRzrN8wyRjOSs9Ipfi1By1H1lHhu2Ry&index=2
[diapositivas-git]: https://tonejito.github.io/curso-git/

## Instalar `git`

Se puede instalar con el administrador de paquetes `apt` o `yum`

- `# apt install git`
- `# yum install git`

## Inicializar los datos del autor

```sh
$ git config user.name  "John Doe"
$ git config user.email "user@example.com"
```

## Clonar el repositorio

```sh
$ git clone https://gitlab.com/PBSC-AdminUNIX/2021/PROYECTO
$ cd PROYECTO
```

## Generar contenido en el repositorio

Crear archivo `README.md`

```
$ cat >> README.md << EOF
# Ejemplo README.md

Integrantes

- NOMBRE
- NOMBRE
	...

## Encabezado nivel 2

Texto de muestra

### Encabezado nivel 3

Imagen de muestra

![Imagen de ejemplo](img/muestra.png)

#### Encabezado nivel 4

Liga hacia script

- [`script.sh`](files/script.sh)

##### Encabezado nivel 5

Tabla

| Encabezado | Otro encabezado | Uno mas    |
|:----------:|:---------------:|:----------:|
| Contenido  | Más contenido   | Otra celda |
| Contenido  | Más contenido   | Otra celda |
| Contenido  | Más contenido   | Otra celda |
```

Este archivo se ve de la siguiente manera cuando está versionado en el repositorio:

- [muestra](../muestra)

### Archivos

+ Copiar archivos existentes al repositorio
+ Abrir los archivos con un editor de texto y hacer cambios

### Directorios

+ Los directorios **vacíos** no se versionan

**ProTip**: Crear el directorio, agregar un archivo con `touch` y versionar ese archivo

```sh
$ mkdir -v directorio/
mkdir: created directory ‘directorio’
$ touch directorio/.keep
$ git add directorio/.keep
```

## Preparar archivos para enviar en el siguiente commit

```sh
$ git add README.md
$ git add img/muestra.png
$ git add files/script.sh
```

## Versionar los cambios

El argumento `-m` se utiliza para ingresar el mensaje del _commit_ sin necesidad de abrir un editor de texto

```sh
$ git commit -m "Cambios en el contenido"
```

## Enviar los cambios versionados al repositorio remoto

```sh
$ git push -u origin master
```

## Hacer más cambios

Edita el archivo `README.md` y agrega más imágenes según sea necesario

## Verificar si alguien más envió cambios al repositorio

```sh
$ git pull
```

## Enviar cambios locales al servidor

```sh
$ git push
```

## Verificar que los cambios estén presentes en el repositorio

Abrir la URL del repositorio en un navegador web

- `https://gitlab.com/PBSC-AdminUNIX/2021/PROYECTO`

Verificar lo siguiente:

- El contenido del archivo `README.md` está presente en el repositorio
- Las imágenes son visibles
- Los archivos adjuntos existen y son accesibles

--------------------------------------------------------------------------------

#### Recursos de ayuda

+ Documentación oficial de `git`

    * <https://git-scm.com/doc>

+ Referencia de comandos de `git`

    * <https://git-scm.com/docs>

+ Libro oficial de `git`

    * <https://git-scm.com/book>

+ Mini tutorial interactivo de git

    * <https://try.github.io/>

+ Hoja de ayuda de comandos de git

    * <https://services.github.com/on-demand/downloads/es_ES/github-git-cheat-sheet/>
    * <https://about.gitlab.com/images/press/git-cheat-sheet.pdf>

+ Páginas de manual (RTFM!):

| `En terminal`		| En línea |
|:----------------------|:---------|
| `man git-config`	| <https://git-scm.com/docs/git-config> |
| `man git-clone`	| <https://git-scm.com/docs/git-clone> |
| `man git-add`		| <https://git-scm.com/docs/git-add> |
| `man git-commit`	| <https://git-scm.com/docs/git-commit> |
| `man git-push`	| <https://git-scm.com/docs/git-push> |
| `man git-pull`	| <https://git-scm.com/docs/git-pull> |

